# simple class for reading and splitting text file with hex data,
import os
from  intelhex import IntelHex

class HexFileSplitter:

    def __init__(self):
        self.__status = {'hexFileOk': False,
                      'hexArrayCompleted':False}
        self.__numberoflines = 0
        self.__lines = []
        self.__currentline = None
        self.__currentchecksumok = True
        self.__hexfilename = ""
        self.__inteHexObj = IntelHex()

    def loadHexfile(self, hexname: str = None)-> None:
        try:
            self.__hexfilename = hexname
            self.__inteHexObj.loadfile(hexname, format='hex')
            self.__status['hexFileOk'] = True
        except(FileNotFoundError):
            self.__inteHexObj = None


        """
         Split file into array of bytes.
        """

    def hexToLines(self):
        processok = True
        try:
            f = open(self.__hexfilename, "r")
            lines = f.readlines()
            for line in lines:
                byteArray = bytes.fromhex(str(line[1:]))
                # should it be in different func module
                #  LSB of a sum of all bytes is 0
                checksum = sum(byteArray) & 0x00FF
                lineDict = {"bytes": byteArray, 'checksumok': True}
                if checksum == 0:
                    lineDict['checksumok'] = True
                else:
                    lineDict['checksumok'] = False
                self.__lines.append(lineDict)

            if len(lines) == len(self.__lines):
                processok = True
            else:
                processok - False

        except(FileNotFoundError):
            processok = False
        finally:
            self.__status['hexArrayCompleted'] = processok

    def updateData(self):
        #todo 4:hex modification
        pass

    def getStatus(self):
        return self.__status

    def getLines(self):
        return self.__lines




