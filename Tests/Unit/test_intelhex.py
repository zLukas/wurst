import os
from unittest import TestCase
from intelhexsplitter import HexFileSplitter

# change dir to use file from current dir
os.chdir('Tests/Unit')
class TestHexFileSplitter(TestCase):

    def test_loadHexfile_load_proper_hex(self):
        # arrange
        testhexsplitter = HexFileSplitter()

        # act
        testhexsplitter.loadHexfile("test.hex")

        # assert
        self.assertTrue(testhexsplitter.getStatus()["hexFileOk"])

    def test_loadHexfile_load_inproper_hex(self):
        # arrange
        testhexsplitter = HexFileSplitter()
        # mock intelhex library

        # act
        testhexsplitter.loadHexfile("test1.hex")

        # assert
        self.assertFalse(testhexsplitter.getStatus()["hexFileOk"])

    def test_hexToLines_proper_hex(self):
        # arrange
        testhexsplitter = HexFileSplitter()
        testhexsplitter.loadHexfile("test.hex")

        #act
        testhexsplitter.hexToLines()

        #assert
        f_test = open("test.hex")
        self.assertEqual(len(f_test.readlines()), len(testhexsplitter.getLines()))
        for line in testhexsplitter.getLines():
            self.assertTrue(line["checksumok"])
        f_test.close()

    def test_updateData_single_byte(self):
        # todo 4: hex modification
        pass

    def test_updateData_multiple_bytes(self):
        # todo 4: hex modification
        pass
    def test_updateData_non_existing_bytes(self):
        # todo 4: hex modification
        pass
